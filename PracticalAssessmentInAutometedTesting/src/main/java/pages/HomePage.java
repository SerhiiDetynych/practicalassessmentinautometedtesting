package pages;

import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.xpath;

/**
 * The type Home page.
 */
public class HomePage extends BasePage {


    private static final String LOGIN_BTN = "//button[@data-testid='login-button']";
    private static final String CONTEXT_MENU_BTN = "//button[@data-testid='user-widget-link']";
    private static final String PROFILE_BTN_IN_CONTEXT_MENU = "//a[contains(@href,'user')]";

    /**
     * Instantiates a new Home page.
     *
     * @param driver the driver
     */
    public HomePage(WebDriver driver) {
        super(driver);
    }

    /**
     * Click on login btn.
     */
    public void clickOnLoginBtn() {
        getDriver().findElement(xpath(LOGIN_BTN)).click();
    }

    /**
     * Click on context menu btn.
     */
    public void clickOnContextMenuBtn() {
        getDriver().findElement(xpath(CONTEXT_MENU_BTN)).click();
    }

    /**
     * Click on profile btn in context menu.
     */
    public void clickOnProfileBtnInContextMenu() {
        getDriver().findElement(xpath(PROFILE_BTN_IN_CONTEXT_MENU)).click();
    }
}
