package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * The type Profile page.
 */
public class ProfilePage extends BasePage {

    private static final String USERNAME = "//span[@class='o4KVKZmeHsoRZ2Ltl078']/h1";

    /**
     * Instantiates a new Profile page.
     *
     * @param driver the driver
     */
    public ProfilePage(WebDriver driver) {
        super(driver);
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return getDriver().findElement(By.xpath(USERNAME)).getText();
    }
}
