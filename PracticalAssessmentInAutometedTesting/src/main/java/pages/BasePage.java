package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * The type Base page.
 */
public class BasePage {

    private WebDriver driver;

    private static final String EXECUTE_SCRIPT_STATE = "return document.readyState";
    private static final String COMPLETE = "complete";

    /**
     * Instantiates a new Base page.
     *
     * @param driver the driver
     */
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Gets driver.
     *
     * @return the driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Implicit wait.
     *
     * @param timeToWait the time to wait
     */
    public void implicitWait(final long timeToWait) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(timeToWait));
    }

    /**
     * Wait for page load complete.
     *
     * @param timeToWait the time to wait
     */
    public void waitForPageLoadComplete(final long timeToWait) {
        new WebDriverWait(driver, Duration.ofSeconds(timeToWait))
                .until(webDriver -> ((JavascriptExecutor) driver).executeScript(EXECUTE_SCRIPT_STATE).equals(COMPLETE));
    }
}
