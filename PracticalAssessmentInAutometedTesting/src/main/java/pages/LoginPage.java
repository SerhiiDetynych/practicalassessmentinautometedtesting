package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.By.*;

/**
 * The type Login page.
 */
public class LoginPage extends BasePage {

    private static final String LOGIN_BTN = "//button[@id='login-button']";
    private static final String WRONG_LOGIN_AND_PASSWORD_MSG = "//span[contains(@class, 'Message')]";
    private static final String LOGIN_INPUT_FIELD = "//input[@id='login-username']";
    private static final String PASSWORD_INPUT_FIELD = "//input[@id='login-password']";

    /**
     * Instantiates a new Login page.
     *
     * @param driver the driver
     */
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Click on login btn.
     */
    public void clickOnLoginBtn() {
        getDriver().findElement(xpath(LOGIN_BTN)).click();
    }

    /**
     * Gets wrong login and password msg.
     *
     * @return the wrong login and password msg
     */
    public WebElement getWrongLoginAndPasswordMsg() {
        return getDriver().findElement(xpath(WRONG_LOGIN_AND_PASSWORD_MSG));
    }

    /**
     * Enter text to login input field.
     *
     * @param login the login
     */
    public void enterTextToLoginInputField(final String login) {
        getDriver().findElement(xpath(LOGIN_INPUT_FIELD)).clear();
        getDriver().findElement(xpath(LOGIN_INPUT_FIELD)).sendKeys(login);
    }

    /**
     * Enter text to password input field.
     *
     * @param password the password
     */
    public void enterTextToPasswordInputField(final String password) {
        getDriver().findElement(xpath(PASSWORD_INPUT_FIELD)).clear();
        getDriver().findElement(xpath(PASSWORD_INPUT_FIELD)).sendKeys(password);
    }
}
