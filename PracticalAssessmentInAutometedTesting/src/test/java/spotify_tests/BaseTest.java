package spotify_tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.HomePage;
import pages.LoginPage;
import pages.ProfilePage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

/**
 * The type Base test.
 */
public class BaseTest {

    private WebDriver driver;

    private static final String SPOTIFY_URL = "https://open.spotify.com/";

    /**
     * Profile set up.
     */
    @BeforeTest
    public void profileSetUp() {
        chromedriver().setup();
    }

    /**
     * Tests set up.
     */
    @BeforeMethod
    public void testsSetUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get(SPOTIFY_URL);
    }

    /**
     * Gets driver.
     *
     * @return the driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Tear down.
     */
    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    /**
     * Gets home page.
     *
     * @return the home page
     */
    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    /**
     * Gets login page.
     *
     * @return the login page
     */
    public LoginPage getLoginPage() {
        return new LoginPage(getDriver());
    }

    /**
     * Gets profile page.
     *
     * @return the profile page
     */
    public ProfilePage getProfilePage() {
        return new ProfilePage(getDriver());
    }

}
