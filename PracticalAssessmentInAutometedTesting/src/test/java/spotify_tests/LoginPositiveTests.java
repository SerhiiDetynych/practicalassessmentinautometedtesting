package spotify_tests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * The type Login tests.
 */
public class LoginPositiveTests extends BaseTest {

    private static final long TIME_TO_WAIT = 60;
    private static final String EXPECTED_USERNAME = "TestUser1";


    /**
     * Check login with correct credential.
     *
     * @param login    the login
     * @param password the password
     */
    @Test
    @Parameters({"login", "password"})
    public void checkLoginWithCorrectCredential(final String login, final String password) {
        getHomePage().clickOnLoginBtn();
        getHomePage().implicitWait(TIME_TO_WAIT);
        getLoginPage().enterTextToLoginInputField(login);
        getLoginPage().enterTextToPasswordInputField(password);
        getLoginPage().clickOnLoginBtn();
        getHomePage().waitForPageLoadComplete(TIME_TO_WAIT);
        getHomePage().clickOnContextMenuBtn();
        getHomePage().clickOnProfileBtnInContextMenu();
        assertEquals(getProfilePage().getUsername(), EXPECTED_USERNAME);
    }
}
