package spotify_tests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * The type Login tests.
 */
public class LoginNegativeTests extends BaseTest {

    private static final long TIME_TO_WAIT = 60;

    /**
     * Check login with incorrect credentials.
     *
     * @param login    the login
     * @param password the password
     */
    @Test
    @Parameters({"login", "password"})
    public void checkLoginWithIncorrectCredentials(final String login, final String password) {
        getHomePage().clickOnLoginBtn();
        getLoginPage().implicitWait(TIME_TO_WAIT);
        getLoginPage().enterTextToLoginInputField(login);
        getLoginPage().enterTextToPasswordInputField(password);
        getLoginPage().clickOnLoginBtn();
        assertTrue(getLoginPage().getWrongLoginAndPasswordMsg().isDisplayed());
    }


}
